from django.db import models

# Create your models here.
class Pengalaman(models.Model):
    nama = models.CharField('Nama', max_length=50)
    cerita = models.TextField('Pengalaman')
    