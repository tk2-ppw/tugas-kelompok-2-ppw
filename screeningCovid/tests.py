from django.test import TestCase, Client
from django.urls import resolve
from .views import bantuan, pendaftar_bantuan
from .models import Isian
import tempfile
from django.http import HttpRequest
from .forms import Form_bantuan

# Create your tests here.
class UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/screening')
        self.assertEqual(response.status_code, 200)

    def test_daftar_using_index_func(self):
        found = resolve('/screening')
        self.assertEqual(found.func, bantuan)
    
    def test_template(self):
        response = Client().get('/screening')
        self.assertTemplateUsed(response,'halscreening.html')

    def test_html_contains(self):
        request = HttpRequest()
        response = bantuan(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Daftar Untuk Penjemputan Pasien Covid', html_response)
        self.assertIn('Nama', html_response)
        self.assertIn('Nomor Hp', html_response)
        self.assertIn('Alamat', html_response)
    
    def test_models(self):
        daftar = Isian(nama='ajay',nohp='082298573854',alamat='jl.bratasena1no7')
        daftar.save()
        daftar1 = Isian.objects.create(nama='ajayy',nohp='082298572154',alamat='jl.bratasena1no6')
        self.assertEqual(Isian.objects.all().count(),2)

class list_bantuan(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/daftarbantuan')
        self.assertEqual(response.status_code, 200)

    def test_list_pendaftar(self):
        found = resolve('/daftarbantuan')
        self.assertEqual(found.func, pendaftar_bantuan)

    def test_template(self):
        response = Client().get('/daftarbantuan')
        self.assertTemplateUsed(response, 'haldaftar.html')

    def test_html_contains(self):
        #pendaftar = Isian.objects.create(nama='azhar', nohp='082298436528', alamat='jl.bratasena1no5')
        request = HttpRequest()
        response = pendaftar_bantuan(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Nama', html_response)
        self.assertIn('Nomor Hp', html_response)
        self.assertIn('Alamat', html_response)
