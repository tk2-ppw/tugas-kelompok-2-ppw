from django.db import models
Tahun_CHOICES = [
    ('RS Pondok Indah', 'RS Pondok Indah - RP900.000'),
    ('RSPAD Gatot Soebroto', 'RSPAD Gatot Soebroto - RP785.000'),
    ('RS Persahabatan', 'RS Persahabatan - RP800.000'),
    ('RS Atma Jaya', 'RS Atma Jaya - RP850.000'),
    ('RS Fatmawati', 'RS Fatmawati - RP865.000'),
    ('RSAL Mintohardjo', 'RSAL Mintohardjo  - RP790.000'),
    ('RS Pertamina Jaya', 'RS Pertamina Jaya - RP820.000'),
    ('RSUD Cengkareng', 'RSUD Cengkareng - RP815.000'),
    ('RSUD Pasar Minggu', 'RSUD Pasar Minggu - RP770.000'),
    ('RS Pelni', 'RS Pelni - RP880.000'),
    ('RSUD Tarakan', 'RSUD Tarakan - RP850.000'),
    ('RSPI Sulianti Saroso', 'RSPI Sulianti Saroso - RP800.000'),
]
Jam_CHOICES = [
    ('08.00', '08.00 WIB'),
    ('09.00', '09.00 WIB'),
    ('10.00', '10.00 WIB'),
    ('11.00', '11.00 WIB'),
    ('13.00', '13.00 WIB'),
    ('14.00', '14.00 WIB'),
    ('15.00', '15.00 WIB'),
    ('16.00', '16.00 WIB'),
]

class Pendaftar(models.Model): 
    nama = models.CharField('Nama', max_length = 30) 
    telp = models.PositiveIntegerField('Nomor HP')
    rumah_sakit = models.CharField('Rumah Sakit', choices=Tahun_CHOICES, max_length=50)
    tanggal = models.DateField('Tanggal', auto_now=False, auto_now_add=False, null=True)
    jam = models.CharField('Jam Pemeriksaan', choices=Jam_CHOICES, max_length=200, null=True)
    Bukti_Transfer = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=100 , null=True)
    last_modified = models.DateTimeField(auto_now_add = True) 
   
        # renames the instances of the model 
        # with their title name 
    def __str__(self): 
        return self.nama
