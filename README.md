Tugas Kelompok 1 PPW
======================

**Nama anggota :**
1. Ammar Faridzki Syarif - 1906353712
2. Azhar Rahmatilah Addzikri - 1906318716
3. Iqfal Fazrial Pramudya - 1906353920
4. Khalishah Aurelia Asbran - 1906318836
5. Muhammad Marsha Prawira - 1906353864

Pipeline Status
-----------------
[![pipeline](https://gitlab.com/Azhar.rahmatilah/tk-2-ppw/badges/master/pipeline.svg)](https://tk1-ppw-20.herokuapp.com)

Berikut adalah **[link website](https://tk2gacoor.herokuapp.com/)** kelompok kami

**Cerita mengenai aplikasi kami dan manfaatnya**
Semakin meningkatnya jumlah positif covid di Indonesia membuat kami ingin melakukan inovasi yang dapat membantu masyarakat, baik dari segi edukasi maupun ekonomi.
Sehingga kami memutuskan untuk membuat Website yang  memiliki beberapa manfaat, yang pertama adalah dapat berdonasi kepada korban covid ataupun orang-orang yang terdampak pada pandemi covid-19. Selain itu juga manfaat dari website kami adalah memberikan informasi mengenai pengalaman dari para pasien covid, baik yang masih dirawat ataupun yang sudah sembuh. Terakhir pada website kamipun terdapat Screening kasar bagi orang yang ingin mengetahui apakah dirinya berpotensi terkena covid-19.

**Daftar fitur yang akan kami implementasikan adalah :**
* **List Rumah Sakit**
  
	Dari fitur ini pengunjung website kami dapat mengetahui lokasi dari berbagai rumah sakit yang ada di daerah Jakarta. Selain lokasinya, pengunjung website kami juga dapat mengetahui nomor telepon dari rumah sakit-rumah sakit tersebut agar dapat memudahkan jika ingin menanyakan sesuatu kepada pihak rumah sakit. Selain itu pada halaman ini pun terdapat fitur yang berfungsi untuk pendaftaran SWAB Test.

* **Screening**
  
	Pengunjung website akan diberikan 2 pertanyaan, yang pertama adalah mengenai apakah pengunjung website tersebut pernah bertemu atau berinteraksi dengan pasien covid, lalu yang kedua adalah apakah pengunjung website tersebut sedang mengalami demam atau batuk pilek. Nantinya pengunjung website akan diberikan informasi sesuai dengan jawaban dari pertanyaan-pertanyaan tersebut.

* **Pengalaman**
  
	Di fitur Pengalaman ini, pengunjung yang pernah atau sedang terkena virus Covid-19 dapat membagikan pengalamannya sendiri saat berusaha melawan virus Covid-19.

* **Riwayat Perjalanan Pasien Covid**
  
	Kebermanfaatan dari fitur Riwayat ini adalah pengunjung website dapat mengetahui beberapa riwayat perjalanan dari orang lain yang terkena virus Covid-19 agar dapat menjauhi daerah-daerah tersebut. Jika pengunjung merupakan salah satu orang yang sedang terkena Covid-19 juga dapat membagikan riwayat perjalanannya jika berkenan agar dapat dilihat pengunjung lain.

* **Donasi**
  
	Selanjutnya ada fitur Donasi, di fitur ini pengunjung dapat mendonasikan sejumlah uang yang nantinya akan diberikan kepada pihak-pihak yang membutuhkan bantuan dalam menangani pandemi virus Covid-19 ini.

* **Info General**
  
	Terakhir adalah fitur Info General, di sini kami akan memaparkan berbagai informasi seperti Apa itu virus Covid-19? Bagaimana cara menghindarinya? Apa yang harus dilakukan jika sudah terkena virus Covid-19? dan lain-lain.


