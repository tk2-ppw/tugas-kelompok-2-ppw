from django.shortcuts import render, redirect
from .forms import Form_Riwayat
from .models import Riwayat
from django.conf import settings
from django.conf.urls.static import static

# Create your views here.
def form_riwayat(request):
    if request.method == 'POST':
        form = Form_Riwayat(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('Riwayat:form_riwayat')
    else:
        form = Form_Riwayat()
    return render(request, 'form_riwayat.html', {'form': form})

def list_riwayat(request):
    hasilForm = Riwayat.objects.all().order_by('-tanggal')
    return render(request, 'list_riwayat.html', {'hasilForm': hasilForm })