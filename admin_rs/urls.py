from django.shortcuts import render

from django.urls import path

from . import views

app_name = 'admin_rs'

urlpatterns = [
	path('login', views.login, name="login"),
	path('login-gagal', views.login_gagal, name="logingagal"),
	path('pilihan-admin', views.pilihan_admin, name="pilihanadmin"),
    path('', views.cobalogin, name='cobalogin'),
]
