from django import forms
from donasi.models import Donatur

# class FormDonatur(forms.Form):
#     nama_donatur = forms.CharField()
#     nominal_donatur = forms.IntegerField()
#     pesan_donatur = forms.CharField()
#     bukti_donatur = forms.ImageField()

class FormDonatur(forms.ModelForm):
    class Meta:
        model = Donatur
        fields = [
            'nama_donatur',
            'nominal_donatur',
            'pesan_donatur',
            'bukti_donatur',
        ]

        widgets = {
            # 'nama_donatur': forms.TextInput(),
            'nominal_donatur': forms.NumberInput(),
            # 'pesan_donatur': forms.TextInput(),
            # 'bukti_donatur': forms.FileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(FormDonatur, self).__init__(*args, **kwargs)
        self.fields['nama_donatur'].widget.attrs['style'] = 'border-radius: 20px; margin:auto;'
        self.fields['nominal_donatur'].widget.attrs['style']  = 'border-radius: 20px;margin:auto'
        self.fields['pesan_donatur'].widget.attrs['style'] = 'border-radius: 20px; margin:auto;'

        self.fields['nama_donatur'].widget.attrs.update({'placeholder': 'Masukkan Nama Anda', 'class':'text-center form-control'})
        self.fields['nominal_donatur'].widget.attrs.update({'placeholder': 'Masukkan Nominal Donasi', 'class':'text-center form-control'})
        self.fields['pesan_donatur'].widget.attrs.update({'placeholder': 'Masukan Pesan Anda', 'class':'text-center form-control'})