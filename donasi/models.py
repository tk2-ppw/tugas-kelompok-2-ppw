from django.db import models

# Create your models here.

class Donatur(models.Model):
    nama_donatur = models.CharField(max_length=100)
    nominal_donatur = models.IntegerField()
    pesan_donatur = models.CharField(max_length=100)
    bukti_donatur = models.ImageField(upload_to= 'donatur/', height_field=None, width_field=None, max_length=100, null=True)

    def getNominal(self):
        return self.nominal_donatur

    def getNama(self):
        return self.nama_donatur

    def __str__(self):
        return "{}. {}".format(self.id, self.nama_donatur)
